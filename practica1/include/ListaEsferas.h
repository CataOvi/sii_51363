#ifndef _INC_LISTAESFERAS_
#define _INC_LISTAESFERAS_

#include "Esfera.h"
#include <vector>
using namespace std;

class ListaEsferas
{
 private:
	int numero;
	vector<Esfera *> lista;
 public:
	bool agregarEsfera(Esfera *);
	//void DestruirContenido();
	void Mueve(float);
	void Dibuja();

	float getDimension(){return lista.size();}
	Esfera & operator[](int i){return *(lista[i]);}//devuelve referencia, por tanto, debo devolver el contenido del puntero

	//void EliminarEsfera(int ind);
	//void EliminarEsfera(Esfera *e);
	//bool colision
};
#endif
