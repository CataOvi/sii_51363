// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// MUNTEAN, CATALIN OVIDIU 51363

Esfera::Esfera():radio_max(0.75f),radio_min(0.25f),pulso(0.1f)
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)//He implementado el mueve
{
	centro=centro+velocidad*t;//funciona gracias a la sobrecarga

	if((radio>radio_max) || (radio<radio_min))
		pulso=-pulso;
	radio+=pulso*t;
}
